1. install docker
2. run command in terminal

    docker run --name postgres-db -e POSTGRES_PASSWORD=postgres_pass -e POSTGRES_USER=postgres -p 5432:5432 -d postgres:14.3

    docker run --name postgres-db-test -e POSTGRES_PASSWORD=postgres_pass -e POSTGRES_USER=postgres -p 6666:5432 -d postgres:14.3

3. additional info:
    #-d means that you enable Docker to run the container in the background
    #-e POSTGRES_USER=postgres is anyway default
