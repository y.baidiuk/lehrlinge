package at.twinformatics.repositories;

import at.twinformatics.entities.Example;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ExampleRepo extends JpaRepository<Example, Long> {
    //we can use CrudRepository but JpaRepo better because include paginator functionality

}
