package at.twinformatics.entities;


import lombok.Data;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // JPA
//@Table(name = "example") // work in low case by default
@Data //generate @Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode.
@RepositoryRestResource(path = "examples")
//@RepositoryRestResource(exported = false) enable to hide it from ip
public class Example {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //by default each fild @Column or @Column(name = "name")
    private String name;


}