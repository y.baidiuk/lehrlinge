package at.twinformatics.controllers;

import at.twinformatics.entities.Example;
import at.twinformatics.repositories.ExampleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController //same like @Controller and each method @ResponseBody
@RequestMapping("examples") //http://localhost:8080/examples/
public class ExampleController {
    @Autowired
    private ExampleRepo exampleRepo;

    // http://localhost:8080/examples/test?name=Wien
    @GetMapping("/test")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        System.out.println("888");
        return String.format("Hello %s!", name);
    }

    // http://localhost:8080/examples
    // but we do not need it because spring data rest will create repositories automaticaly
    @GetMapping()
    public ResponseEntity<List<Example>> getExamples() {
        System.out.println("HELLOOOOOO");
        List<Example> result = exampleRepo.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
