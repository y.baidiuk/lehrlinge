package at.twinformatics.config;

import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResetDbConfig {

    @Bean
    public FlywayMigrationStrategy cleanDb() {
        return flyway -> {
            flyway.clean();
            flyway.migrate();
        };
    }
}
