package at.twinformatics.controllers;

import at.twinformatics.entities.Example;
import at.twinformatics.repositories.ExampleRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
class ControllerExampleTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ExampleRepo exampleRepo;

    private static Example example;

    @Test
    public void getExampleTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/examples/test?name=test"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("Hello test!")));
    }

    @Test
    @Order(1)
    void createTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/examples")
                        .accept("application/hal+json")
                        .content("{\"name\": \"3\"}"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"name\" : \"3\"")));
        List<Example> list = exampleRepo.findAll();

        //check in db
        Assertions.assertEquals(1, list.size());
        example = list.get(0);
    }

    @Test
    @Order(2)
    void updateTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .put("/api/examples/" + example.getId())
                        .accept("application/hal+json")
                        .content("{\"name\": \"newName\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"name\" : \"newName\"")));

        mvc.perform(MockMvcRequestBuilders
                        .patch("/api/examples/" + example.getId())
                        .accept("application/hal+json")
                        .content("{\"name\": \"newName2\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"name\" : \"newName2\"")));

        Optional<Example> entityFromDb = exampleRepo.findById(example.getId());
        Assertions.assertTrue(entityFromDb.isPresent());
        Assertions.assertEquals("newName2", entityFromDb.get().getName());
    }
    @Test
    @Order(3)
    void getTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .get("/api/examples/")
                        .accept("application/hal+json"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"name\" : \"newName2\"")));

        mvc.perform(MockMvcRequestBuilders
                        .get("/api/examples/" + example.getId())
                        .accept("application/hal+json")
                        .content("{\"name\": \"newName2\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().string(containsString("\"name\" : \"newName2\"")));
    }
    @Test
    @Order(4)
    void deleteTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .delete("/api/examples/" + example.getId())
                        .accept("application/hal+json"))
                .andExpect(status().isNoContent());
        Assertions.assertEquals(0, exampleRepo.findAll().size());
    }

    // this test should be moved inside ExmpleRepoTest because it tests auto-created controllers and repo as well.
    // repo test can be removed
}
