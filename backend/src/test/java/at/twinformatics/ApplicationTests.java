package at.twinformatics;

import at.twinformatics.repositories.ExampleRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTests {
    @Autowired
    ExampleRepo exampleRepo;

    @Test
    void checkAutowired() {
        Assertions.assertNotNull(exampleRepo);
    }

}
