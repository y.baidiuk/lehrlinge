package at.twinformatics.repositories;

import at.twinformatics.entities.Example;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;


@SpringBootTest
class ExampleRepoTest {

    @Autowired
    private ExampleRepo exampleRepo;

    private static Example example;

    @Test
    void createAndReadTest() {
        example = new Example();
        example.setName("a");
        exampleRepo.save(example);
        exampleRepo.save(new Example());
        exampleRepo.save(new Example());
        Assertions.assertEquals(3, exampleRepo.findAll().size());
    }

    @Test
    void updateTest() {
        example.setName("newName");
        exampleRepo.save(example);
        Optional<Example> entityFromDb = exampleRepo.findById(example.getId());
        Assertions.assertTrue(entityFromDb.isPresent());
        Assertions.assertEquals(example.getName(), entityFromDb.get().getName());
        Assertions.assertEquals(example, entityFromDb.get());
    }

    @Test
    void crudTest3() {
        exampleRepo.deleteAll();
        Assertions.assertEquals(0, exampleRepo.findAll().size());
    }

}