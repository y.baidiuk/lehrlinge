import {Component, OnInit} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Example} from './interfaces/Example'
import {Response} from './interfaces/Response'


const URL_EXAMPLE = 'http://localhost:8080/api/examples'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  exampleList?: Example[]

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.updateList()
  }

  private updateList() {
    this.http.get<Response<Example>>(URL_EXAMPLE + '?size=50')
      .subscribe(response => this.exampleList = response?._embedded?.examples)
  }

  addExample(name: string) {
    this.http.post(URL_EXAMPLE, {name})
      .subscribe(
        _ => {
          this.updateList()
        })
  }



}
