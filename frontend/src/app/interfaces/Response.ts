export interface Response<T> {
  _embedded: {
    examples: T [];
  },
  _links: {
    self: {
      href: string
    },
    profile: {
      href: string
    }
  },
  page: {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
  }
}
