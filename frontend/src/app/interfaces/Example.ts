export interface Example {
  id: number;
  name: string;
  _links: {
    self: {
      href: string
    },
    example: {
      href: string
    }
  }
}
