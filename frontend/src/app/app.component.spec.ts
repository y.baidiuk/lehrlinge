import {TestBed} from '@angular/core/testing'
import {AppComponent} from './app.component'
import {HttpClient} from '@angular/common/http'
import {of} from 'rxjs'

describe('AppComponent', () => {
  let fixture: any //manipulate lifecycle (.detectChanges())
  let component: any
  let spy = jasmine.createSpyObj(['get', 'post'])

  // setup dependencies
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      // imports: [HttpClientModule],do not need because we use spy (mock)
      providers: [{provide: HttpClient, useValue: spy}],
      declarations: [
        AppComponent
      ]
    }).compileComponents()
  })

  // setup component
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent)
    component = fixture.componentInstance
    spy.get.calls.reset()//reset previous calls
    spy.post.calls.reset()
  })

  it('should create the app', () => {
    expect(component).toBeTruthy()
  })

  it('should call getAllExamples', () => {
    spy.get.and.returnValue(of(true))
    fixture.detectChanges()//call onInit
    expect(spy.get).toHaveBeenCalledTimes(1)
  })

  it('should add new example', () => {
    spy.post.and.returnValue(of(true))

    component.addExample('new')
    expect(spy.post).toHaveBeenCalledTimes(1)
    expect(spy.get).toHaveBeenCalledTimes(1)
  })
})
