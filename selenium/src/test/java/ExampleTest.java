import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


//run backend with test db
//run frontend
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExampleTest {
    static WebDriver driver;

    @BeforeAll
    static void beforeAll() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @AfterAll
    static void afterAll() {
        //close browser
        driver.quit();
    }

    @Test
    @Order(0)
    void add() throws InterruptedException {
        driver.get("http://localhost:4200");
        driver.findElement(By.cssSelector("input")).sendKeys("Hello Test");
        driver.findElement(By.cssSelector("button")).click();
        driver.findElement(By.cssSelector("input")).clear();
        driver.findElement(By.cssSelector("input")).sendKeys("Hello Test2");
        driver.findElement(By.cssSelector("button")).click();

        Thread.sleep(150);

        int itemsSize = driver.findElements(By.cssSelector("li")).size();
        Assertions.assertTrue(itemsSize >= 2);
        Assertions.assertTrue(driver.findElements(By.cssSelector("li"))
                .get(itemsSize - 1)
                .getText()
                .contains("Hello Test2"));
    }

    @Order(1)
    @Test
    void remove() {
        //todo
    }

    @Order(2)
    @Test
    void getAll() {
        int itemsSize = driver.findElements(By.cssSelector("li")).size();
        Assertions.assertTrue(itemsSize >= 2);
    }
}